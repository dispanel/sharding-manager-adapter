module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  forceCoverageMatch: ["src/**/*.unit.test.ts", "tests/**/*.int.test.ts"]
};