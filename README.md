# Sharding Manager Adapter - DiscordJS

This module is an adapter on top of `DiscordJS.ShardingManager`.

It helps to perform any process without bugs, more elegant and faster without worrying about what a broadcast produced, if you had a syntax error on an eval, how ugly the broadcast evals look or having to worry on how to make your own Sharding Manager Adapter.

## Making your first app

```js
// index.js
import { ShardingManagerAdapter } from '@dispanel/sharding-manager-adapter';

// Instantiate Sharding Manager Adapter singleton
const token = 'my_private_bot_token';
const clientDir = './src/client.js';
const totalShards = 4;
ShardingManagerAdapter.createInstace(clientDir, token, totalShards);

// Get all categories
async () => {
  const categories = await ShardingManagerAdapter
                      .client
                      .guilds
                      .categories.getAll('guildID here');
  console.log(categories);
};
// Create a new channel
async () => {
  const newChannel = await ShardingManagerAdapter
                            .client
                            .guilds
                            .channels.create('guildID Here', 'channel name')
});

// client.js
import { Client } from 'discord.js';

// Shard arguments
const token = process.argv[2];
const totalShards = process.argv[3];

// Client instantiation
const client = new Client();

// Login with the token provided to the shard by the ShardingManagerAdapter
client.login(token)
};
```

## Docs

----
### ShardingManagerAdapter

#### createInstance (clientPath: string, token: string, totalShards: number) : Promise<ShardingManager>

Creates a new singleton `DiscordJS.ShardingManager` instance and automatically spawns all the shards.

#### getInstance () : Promise<ShardingManager>

##### Description

Returns the ShardingManager singleton instance. 

At most, you wont need to use this unless you want to do something manually.

----

### ShardingManagerAdapter.client
***Nothing yet***

----

### ShardingManagerAdapter.client.guilds

#### get (guildID: string) : Promise\<Guild>

Returns the guild with the specified guild ID

#### getAll() : Promise<Guild[]>

#### getName(guildID: string) : Promise\<string>

#### setName(guildID: string, name: string) : Promise\<Guild>

----

### ShardingManagerAdapter.client.guilds.channels

#### create (guildID: string, name: string, options: DiscordJS.GuildCreateChannelOptions) : Promise\<Channel>

#### get (guildID: string) : Promise\<GuildChannel>

#### getAll() : Promise<GuildChannel[]>

#### getName(guildID: string) : Promise\<string>

#### setName(guildID: string, name: string) : Promise\<Channel>

----

### ShardingManagerAdapter.client.guilds.categories

#### create (guildID: string, name: string, options: string) : Promise\<Category>

#### get (guildID: string) : Promise\<Category>

#### getAll() : Promise<Category[]>

#### getName(guildID: string) : Promise\<string>

#### setName(guildID: string, name: string) : Promise\<Category>