import {ShardingManager} from 'discord.js';

import {Client} from './Client';
import {Utils} from './utils';

export class ShardingManagerAdapter {
  static client = new Client();
  private static shardingManager: ShardingManager|undefined = undefined;

  static async getInstance(): Promise<ShardingManager> {
    if (this.shardingManager) {
      return this.shardingManager;
    }
    throw new Error(
        'No instance has been created. Use `ShardingManagerAdapter.createInstance()` to create an instance.');
  }
  static async createInstance(
      clientDirectory: string, token: string,
      totalShards: number): Promise<ShardingManager> {
    if (this.shardingManager) {
      throw new Error('A sharding manager already exists.');
    }
    this.shardingManager = new ShardingManager(
        clientDirectory, {token, shardArgs: [token, totalShards.toString()]});

    await this.shardingManager.spawn(totalShards);

    return this.shardingManager;
  }
  static async broadcastEval(evalString: string): Promise<unknown[]> {
    try {
      const shardingManager = await this.getInstance();
      const evalValue = await shardingManager.broadcastEval(evalString);
      const flatEvalValue = Utils.flattenArray(evalValue);
      return flatEvalValue;
    } catch (e) {
      throw e;
    }
  }
}