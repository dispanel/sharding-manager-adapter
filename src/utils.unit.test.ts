import { Utils } from './utils';

describe('Utils', () => {
  describe('flattenArray', () => {
    test('should return flat version of the array', () => {
      // Non flat array
      const nonFlatArray = [
        [
          {key: 'value'},
          {key: 'value2'}
        ],
        [
          {key: 'value3'}
        ]
      ];
      // Flat array
      const flatArray = [
        {key: 'value'},
        {key: 'value2'},
        {key: 'value3'}
      ];
      // Get flat array
      const result = Utils.flattenArray(nonFlatArray);
      // Test values
      expect(result).toEqual(flatArray);
    });
    test('should return flat array without undefined values', () => {
      // Non flat array
      const nonFlatArray = [
        [
          {key: 'value'},
          {key: 'value2'},
        ],
        undefined,
        [
          {key: 'value3'}
        ],
        undefined
      ];
      // Flat array
      const flatArray = [
        {key: 'value'},
        {key: 'value2'},
        {key: 'value3'}
      ];
      // Get flat array
      const result = Utils.flattenArray(nonFlatArray);
      // Test values
      expect(result).toEqual(flatArray);
    });
    test('should return flat array without null and undefined values', () => {
      // Non flat array
      const nonFlatArray = [
        [
          {key: 'value'},
          {key: 'value2'},
        ],
        null,
        [
          {key: 'value3'}
        ],
        undefined
      ];
      // Flat array
      const flatArray = [
        {key: 'value'},
        {key: 'value2'},
        {key: 'value3'}
      ];
      // Get flat array
      const result = Utils.flattenArray(nonFlatArray);
      // Test values
      expect(result).toEqual(flatArray);
    });
    test('should return flat array with false values', () => {
      // Non flat array
      const nonFlatArray = [
        [
          {key: 'value'},
          {key: 'value2'},
        ],
        [false],
        [
          {key: 'value3'}
        ],
        [true]
      ];
      // Flat array
      const flatArray = [
        {key: 'value'},
        {key: 'value2'},
        false,
        {key: 'value3'},
        true
      ];
      // Get flat array
      const result = Utils.flattenArray(nonFlatArray);
      // Test values
      expect(result).toEqual(flatArray);
    });
  });
});