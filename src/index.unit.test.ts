import { ShardingManagerAdapter } from './';
import { ShardingManager } from 'discord.js';
import { Utils } from './utils';

describe('ShardingManagerAdapter', () => {
  it('broadcastEval()', async () => {
    // Non flat array returned by shardingManager.broadcastEval
    const nonFlatArray = [
      [
        {name: 'test guild1'},
        {name: 'test guild2'}
      ],
      [
        {name: 'test guild3'}
      ]
    ];
    // Flat array
    const flatArray = Utils.flattenArray(nonFlatArray);
    // Mock ShardingManagerAdapter.getInstance
    const getInstance = jest.spyOn(ShardingManagerAdapter, 'getInstance');
    getInstance.mockImplementation(
      async (): Promise<ShardingManager> => {
        // Sharding Manager Instance
        const shardingManager = new ShardingManager('./build/src/index.js');
        // Mocking sharding manager broadcastEval
        const broadcastEval = jest.spyOn(shardingManager, 'broadcastEval');
        broadcastEval.mockImplementation(
          async (evalString: string): Promise<unknown[]> => {
            return nonFlatArray;
          }
        );
        // Returning mocked shardingManager
        return shardingManager;
      }
    );
    // ShardingManagerAdapter.broadcastEval result
    const broadcastResult = await ShardingManagerAdapter.broadcastEval('test');
    // Test values
    expect(broadcastResult).toEqual(flatArray);
    expect(getInstance.mock.calls.length).toEqual(1);
  });
});