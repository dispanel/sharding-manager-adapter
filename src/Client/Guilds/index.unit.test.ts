import { ShardingManagerAdapter } from '../../';
import { Guilds } from './';

describe('Client.Guilds', () => {
  describe('getName()', () => {
    it('should return "Dispanel"', async () => {
      // Mock ShardingManagerAdapter.broadcastEval
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          return [{name: 'test name'}];
        });
      // Guilds instance
      const guilds = new Guilds();
      // Get guild's name
      const name = await guilds.getName('test id');
      // Test values
      expect(name).toEqual('test name');
      expect(broadcastEval.mock.calls.length).toEqual(1);
      // Restore mockup function to original
      broadcastEval.mockRestore();
    });
  });
  describe('setName()', () => {
    it('should return the updated guild', async () => {
      // Mocking ShardingManagerAdapter.broadcastEVal
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          return [{name: 'new test name'}];
        }
      );
      // Guilds instance
      const guilds = new Guilds();
      // Mocking Guilds.exists
      const exists = jest.spyOn(guilds, 'exists');
      exists.mockImplementation(
        async (guildID: string): Promise<boolean> => {
          return true;
        }
      );
      // Set new name
      const updatedGuild = await guilds.setName('test id', 'test name');
      // Test values
      expect(updatedGuild).toEqual({name: 'new test name'});
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
      exists.mockRestore();
    });
  });
  describe('get()', () => {
    it('should return a guild', async () => {
      // Mock ShardingManagerAdapter.broadcastEval
      const broadcastEval = jest.spyOn (ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          return [{name: 'test name'}];
        }
      );
      // Guilds instance
      const guilds = new Guilds();
      // Get guild
      const guild = await guilds.get('test id');
      // Test values
      expect(guild).toEqual({name: 'test name'});
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
  describe('getAll()', () => {
    it('should return all guilds', async () => {
      // Guilds array
      const guildsArray = [
        {name: 'test name'},
        {name: 'test name2'}
      ];
      // Mock ShardingManagerAdapter.broadcastEval
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          return guildsArray;
        }
      );
      // Guilds instance
      const guilds = new Guilds();
      // Get all guilds
      const allGuilds = await guilds.getAll();
      // Test Values
      expect(allGuilds).toEqual(guildsArray);
      expect(broadcastEval.mock.calls.length).toEqual(1);
      // Restore mocked function to original
      broadcastEval.mockRestore();
    });
  });
  describe('exists()', () => {
    it('should return true', async () => {
      // Mock ShardingManagerAdapter.broadcastEval
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          return [false, true];
        }
      );
      // Guilds instance
      const guilds = new Guilds();
      // Get exists result
      const exists = await guilds.exists('test id');
      // Test values
      expect(exists).toEqual(true);
      expect(broadcastEval.mock.calls.length).toEqual(1);
      // Restore mocked function to original
      broadcastEval.mockRestore();
    });
  });
});