import { ShardingManagerAdapter } from '../../../';
import { Categories } from './';

describe('Client.Guilds.Categories', () => {
  describe('create()', () => {
    it('should return the category that was created', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          const category = {
            guild: {
              id: 'test id',
            },
            name: 'test name'
          };

          return [category];
        }
      );

      const categories = new Categories();
      const categoryChannelCreated = await categories.create('test id', 'test name');

      expect(categoryChannelCreated).toEqual({guild: {id: 'test id'}, name: 'test name'});
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
  describe('getName()', () => {
    it('should return name "Dispanel"', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
          async (evalString: string): Promise<unknown[]> => {
            const category = {name: 'Dispanel'};

            return [category];
          });

      const categories = new Categories();
      const name = await categories.getName('test id', 'test id');

      expect(name).toEqual('Dispanel');
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
  describe('setName()', () => {
    it('should return the category with updated name', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          const category = { name: 'new test name' };

          return [category];
        }
      );
      
      const categories = new Categories();

      const exists = jest.spyOn(categories, 'exists');
      exists.mockImplementationOnce(async (guildID: string): Promise<boolean> => {
        return true;
      });

      const category = await categories.setName('test id', 'test id', 'new test name');

      expect(category).toEqual({name: 'new test name'});
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
      exists.mockRestore();
    });
  });
  describe('get()', () => {
    it('should return a category', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          const category = { name: 'test name' };

          return [category];
        }
      );
      
      const categories = new Categories();
      const category = await categories.get('test id', 'test id');

      expect(category).toEqual({name: 'test name'});
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
  describe('getAll()', () => {
    it('should return categories', async () => {
      const categoriesArray = [
        { name: 'test name'},
        { name: 'test name2'}
      ];
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          return categoriesArray;
        }
      );
      
      const categories = new Categories();
      const allCategories = await categories.getAll('test id');

      expect(allCategories).toEqual(categoriesArray);
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
  describe('exists()', () => {
    it('should return true', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          return [true];
        }
      );

      const categories = new Categories();
      const exists = await categories.exists('test id', 'test id');

      expect(exists).toEqual(true);
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
});