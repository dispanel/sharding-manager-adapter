import {CategoryChannel, GuildChannel, GuildCreateChannelOptions} from 'discord.js';

import {ShardingManagerAdapter} from '../../../';

interface CategoryCreateOptions extends GuildCreateChannelOptions {
  type: 'category';
}

export class Categories {
  async create(guildID: string, name: string, options?: CategoryCreateOptions):
      Promise<GuildChannel> {
    try {
      // Name eval
      const nameEval = `'${name}'`;
      // Deserialize options object if existant
      const optionsEval = options ? ', '+JSON.stringify(options) : '';
      // Eval script string
      const evalString = `
      const guild = this.guilds.get('${guildID}');
      if (guild) {
        if (guild.available) {
          guild.channels.create(${nameEval} ${optionsEval});
        }
      }
      `;
      // Broadcast eval script to ShardingManager
      const evalResult = await ShardingManagerAdapter.broadcastEval(
                             evalString) as GuildChannel[];
      // Result stored in first array position
      const category = evalResult[0];
      
      return category;
    } catch (e) {
      throw e;
    }
  }

  async getName(guildID: string, categoryID: string): Promise<string> {
    try {
      const category = await this.get(guildID, categoryID);
      return category.name;
    } catch (e) {
      throw e;
    }
  }

  async setName(guildID: string, categoryID: string, name: string):
      Promise<GuildChannel> {
    try {
      const categoryExists = await this.exists(guildID, categoryID);
      if (categoryExists) {
        try {
          const evalString = `
          const guild = this.guilds.get('${guildID}');
          if (guild) {
            if (guild.available) {
              const category = guild.channels
                .filter(channel => channel.type === 'category')
                .get('${categoryID}');
              if (category) {
                category.setName('${name}');
              }
            }
          }
          `;
          const evalResult =
              await ShardingManagerAdapter.broadcastEval(evalString);

          if (evalResult.length > 0) {
            const category = evalResult[0] as GuildChannel;
            return category;
          }

          throw new Error(`Couldn't update guild's name`);
        } catch (e) {
          throw e;
        }
      } else {
        throw new Error('Guild doesn\'t exists');
      }
    } catch (e) {
      throw e;
    }
  }

  async get(guildID: string, categoryID: string): Promise<CategoryChannel> {
    const evalString = `
    const guild = this.guilds.get('${guildID}');
    if (guild) {
      if (guild.available) {
        guild.channels
          .filter(channel => channel.type === 'category')
          .get(${categoryID});
      }
    }
    `;
    const evalResultsGuild =
        await ShardingManagerAdapter.broadcastEval(evalString);
    const category: CategoryChannel = evalResultsGuild[0] as CategoryChannel;
    if (category) {
      return category;
    }
    throw new Error('Guild doesn\'t exist');
  }

  async getAll(guildID: string): Promise<CategoryChannel[]> {
    const evalString = `
    const guild = this.guilds.get('${guildID}');
    if (guild) {
      if (guild.available) {
        guild.channels
          .filter(channel => channel.type === 'category');
      }
    }
    `;
    const evalResultsGuild =
        await ShardingManagerAdapter.broadcastEval(evalString);
    const guilds: CategoryChannel[] = evalResultsGuild as CategoryChannel[];
    return guilds;
  }

  async exists(guildID: string, categoryID: string): Promise<boolean> {
    try {
      const evalString = `
      const guild = this.guilds.get('${guildID}');
      if (guild) {
        if (guild.available) {
          guild.channels
            .filter(channel => channel.type === 'category')
            .has('${categoryID});
        }
      }`;
      const evalResultsExists =
          await ShardingManagerAdapter.broadcastEval(evalString);
      const exists = evalResultsExists[0] as boolean;

      return exists;
    } catch (e) {
      throw e;
    }
  }
}