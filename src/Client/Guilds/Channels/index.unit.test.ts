import { ShardingManagerAdapter } from '../../../';
import { Channels } from './';

describe('Client.Guilds.Channels', () => {
  describe('create()', () => {
    it('should return the channel that was created', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          const channel = {
            guild: {
              id: 'test id',
            },
            name: 'test name'
          };

          return [channel];
        }
      );

      const channels = new Channels();
      const channelChannelCreated = await channels.create('test id', 'test name');

      expect(channelChannelCreated).toEqual({guild: {id: 'test id'}, name: 'test name'});
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
  describe('getName()', () => {
    it('should return name "Dispanel"', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
          async (evalString: string): Promise<unknown[]> => {
            const channel = {name: 'Dispanel'};

            return [channel];
          });

      const channels = new Channels();
      const name = await channels.getName('test id', 'test id');

      expect(name).toEqual('Dispanel');
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
  describe('setName()', () => {
    it('should return the channel with updated name', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          const channel = { name: 'new test name' };

          return [channel];
        }
      );
      
      const channels = new Channels();

      const exists = jest.spyOn(channels, 'exists');
      exists.mockImplementationOnce(async (guildID: string): Promise<boolean> => {
        return true;
      });

      const channel = await channels.setName('test id', 'test id', 'new test name');

      expect(channel).toEqual({name: 'new test name'});
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
      exists.mockRestore();
    });
  });
  describe('get()', () => {
    it('should return a channel', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          const channel = { name: 'test name' };

          return [channel];
        }
      );
      
      const channels = new Channels();
      const channel = await channels.get('test id', 'test id');

      expect(channel).toEqual({name: 'test name'});
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
  describe('getAll()', () => {
    it('should return channels', async () => {
      const channelsArray = [
        { name: 'test name'},
        { name: 'test name2'}
      ];
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          return channelsArray;
        }
      );
      
      const channels = new Channels();
      const allCategories = await channels.getAll('test id');

      expect(allCategories).toEqual(channelsArray);
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
  describe('exists()', () => {
    it('should return true', async () => {
      const broadcastEval = jest.spyOn(ShardingManagerAdapter, 'broadcastEval');
      broadcastEval.mockImplementation(
        async (evalString: string): Promise<unknown[]> => {
          return [true];
        }
      );

      const channels = new Channels();
      const exists = await channels.exists('test id', 'test id');

      expect(exists).toEqual(true);
      expect(broadcastEval.mock.calls.length).toEqual(1);
      broadcastEval.mockRestore();
    });
  });
});