import {GuildChannel, GuildCreateChannelOptions} from 'discord.js';

import {ShardingManagerAdapter} from '../../../';

export class Channels {
  async create(
      guildID: string, name: string,
      options?: GuildCreateChannelOptions): Promise<GuildChannel> {
    try {
      // Name eval
      const nameEval = `'${name}'`;
      // Deserialize options object if existant
      const optionsEval = options ? ', '+JSON.stringify(options) : '';
      // Eval script string
      const evalString = `
      const guild = this.guilds.get('${guildID});
      if (guild) {
        if (guild.available) {
          guild.channels.create(${nameEval} ${optionsEval});
        }
      }
      `;
      const evalResult = await ShardingManagerAdapter.broadcastEval(
                              evalString) as GuildChannel[];
      const channel = evalResult[0];

      return channel;
    } catch (e) {
      throw new Error(e);
    }
  }

  async getName(guildID: string, channelID: string): Promise<string> {
    try {
      const channel = await this.get(guildID, channelID);
      return channel.name;
    } catch (e) {
      throw new Error(e);
    }
  }

  async setName(guildID: string, channelID: string, name: string):
      Promise<GuildChannel> {
    try {
      const channelExists = await this.exists(guildID, channelID);
      if (channelExists) {
        try {
          const evalString = `
          const guild = this.guilds.get('${guildID}');
          if (guild) {
            if (guild.available) {
              const channel = guild.channels.get('${channelID}');
              if (channel) {
                channel.setName('${name}');
              }
            }
          }
          `;
          const evalResults = await ShardingManagerAdapter.broadcastEval(
                                  evalString) as GuildChannel[];
          if (evalResults.length !== 0) {
            const channel = evalResults[0];
            return channel;
          }
          throw new Error(`Couldn't update guild's name`);
        } catch (e) {
          throw new Error(e);
        }
      } else {
        throw new Error('Guild doesn\'t exists');
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  async get(guildID: string, channelID: string): Promise<GuildChannel> {
    try {
      const evalString = `
      const guild = this.guilds.get('${guildID}');
      if (guild) {
        if (guild.available) {
          guild.channels.get('${channelID}'));
        }
      }
      `;
      const evalResultsChannel =
          await ShardingManagerAdapter.broadcastEval(evalString);
      const channel: GuildChannel = evalResultsChannel[0] as GuildChannel;
      if (channel) {
        return channel;
      }
      throw new Error('Guild doesn\'t exist');
    } catch (e) {
      throw new Error(e);
    }
  }
  async getAll(guildID: string): Promise<GuildChannel[]> {
    try {
      const evalString = `
      const guild = this.guilds.get('${guildID}');
      if (guild) {
        if (guild.available) {
          guild.channels;
        }
      }
      `;
      const evalResultsChannels =
          await ShardingManagerAdapter.broadcastEval(evalString);
      const channels: GuildChannel[] = evalResultsChannels as GuildChannel[];
      return channels;
    } catch (e) {
      throw new Error(e);
    }
  }
  async exists(guildID: string, channelID: string): Promise<boolean> {
    try {
      const evalString = `
      const guild = this.guilds.get('${guildID}');
      if (guild) {
        if (guild.available) {
          guild.channels.has('${channelID}');
        }
      }
      `;
      const evalResultsExists =
          await ShardingManagerAdapter.broadcastEval(evalString);
      const exists = evalResultsExists[0] as boolean;
      return exists;
    } catch (e) {
      throw new Error(e);
    }
  }
}