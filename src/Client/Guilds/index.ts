import {Guild} from 'discord.js';

import {ShardingManagerAdapter} from '../../';

import {Categories} from './Categories';
import {Channels} from './Channels';

export class Guilds {
  channels = new Channels();
  categories = new Categories();

  async getName(guildID: string): Promise<string> {
    try {
      const guild = await this.get(guildID);
      return guild.name;
    } catch (e) {
      throw e;
    }
  }

  async setName(guildID: string, name: string): Promise<Guild> {
    try {
      const guildExists = await this.exists(guildID);
      if (guildExists) {
        try {
          const evalString = `
          const guild = this.guilds.get('${guildID}');
          if (guild) {
            if (guild.available) {
              guild.setName('${name}');
            }
          }`;
          const evalResults =
              await ShardingManagerAdapter.broadcastEval(evalString);
          if (evalResults.length !== 0) {
            const channel = evalResults[0] as Guild;
            return channel;
          }
          throw new Error(`Couldn't update guild's name`);
        } catch (e) {
          throw e;
        }
      } else {
        throw new Error('Guild doesn\'t exists');
      }
    } catch (e) {
      throw e;
    }
  }

  async get(guildID: string): Promise<Guild> {
    const evalString = `this.guilds.get('${guildID}')`;
    const evalResultsGuild =
        await ShardingManagerAdapter.broadcastEval(evalString);
    const guild: Guild = evalResultsGuild[0] as Guild;
    if (guild) {
      return guild;
    }
    throw new Error('Guild doesn\'t exist');
  }

  async getAll(): Promise<Guild[]> {
    const evalString = `this.guilds`;
    const evalResultsGuild =
        await ShardingManagerAdapter.broadcastEval(evalString);
    const guilds: Guild[] = evalResultsGuild as Guild[];
    return guilds;
  }

  async exists(guildID: string): Promise<boolean> {
    try {
      const evalString = `this.guilds.has('${guildID}')`;
      const evalResultsExists =
          await ShardingManagerAdapter.broadcastEval(evalString);
      const indexOfTrue = evalResultsExists.indexOf(true);
      const exists = indexOfTrue >= 0;

      return exists;
    } catch (e) {
      throw e;
    }
  }
}