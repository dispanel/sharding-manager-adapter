export class Utils {
  static flattenArray(arg: unknown): unknown[] {
    if (!(arg instanceof Array)) {
      return [];
    }

    return arg.reduce((acc, arr) => {
      acc = acc instanceof Array ? acc : [];
      arr = arr instanceof Array ? arr : arr ? [arr] : [];

      return [...acc, ...arr];
    });
  }
}